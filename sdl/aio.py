# SDL by Sven Skobowsky

# To the extent possible under law, the person who associated CC0 with SDL
# has waived all copyright and related or neighboring rights to SDL.

# You should have received a copy of the CC0 legalcode along with this
# work.  If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.


import asyncio
import collections as col
import functools
import logging
import time


logger = logging.getLogger(__name__)


def run(awaitable):  # TODO remove and use asnycio.run instead if Python min version >= 3.7
    """ Execute async (test) methods by providing scaffolding looping code.

    This is a basically a workaround for not having unittest.IsolatedAsyncioTestCase

    >>> import unittest
    >>> class Foo(unittest.TestCase):
    ...     @run
    ...     async def test(self):  # a (test) coroutine ...
    ...         # ... await stuff
    ...         self.assertTrue(False)
    >>> Foo().test()  # ... which can be called like a common function
    Traceback (most recent call last):
        ...
    AssertionError: False is not true
    """
    @functools.wraps(awaitable)
    def decor(*args, **kwargs):
        loop = asyncio.new_event_loop()
        loop.run_until_complete(awaitable(*args, **kwargs))
        loop.close()
    return decor


def throttle(period_in_seconds, n_calls_in_period=1):
    """ Apply a minimum time between consecutive calls to a function.

    A delay of period_in_seconds (p) will be ensured between every n_calls_in_period-th (n-th)
    call. This way no more than n calls are executed within p, but n calls can be done
    simultaneously.
    Example: p = 10, n = 5; 3 calls issuing every 6 time units

        x     x     x     x     x                       x   ... executing a call
        x     x     x     :-x   :-x                     :-- ... delayed call
        x     :---x :---x :---x :---x
        |----+----+----+----+----+----> time
        0 .. 5 .. 10.. 15.. 20.. 25..

    :param period_in_seconds: min waiting time between calls
    :param n_calls_in_period: number of maximum calls allowed within period_in_seconds
    """
    if n_calls_in_period < 1:
        raise ValueError(f'n_calls_in_period must be greater than 0 but was {n_calls_in_period}')

    next_call_time = col.deque([-float('inf')], n_calls_in_period)

    def arg_decor(awaitable):
        @functools.wraps(awaitable)
        async def decor(*args, **kwargs):
            nonlocal next_call_time

            now = time.time()
            if now < next_call_time[0]:
                delay = next_call_time[0] - now
                logger.debug(f'Calling {awaitable.__name__} too eagerly - delaying by {delay:.3g} seconds.')
                next_call_time.append(next_call_time[0] + period_in_seconds)
                await asyncio.sleep(delay)
            else:
                next_call_time.append(now + period_in_seconds)

            return await awaitable(*args, **kwargs)
        return decor
    return arg_decor


def retry(n_attempts=-1, exception=RuntimeError):
    """ Repeat function call if it raises a specific exception.

    :example:
    >>> count = 3
    >>> @run
    ... @retry()
    ... async def func():
    ...     global count
    ...     count -= 1
    ...     print('Tick,')
    ...     if count:
    ...         raise RuntimeError()
    ...     print('Boom!')
    >>> func()
    Tick,
    Tick,
    Tick,
    Boom!

    :param n_attempts: maximum number of calls, initial one included; infinite retries by default
                       and negative values
    :param exception:  exception type which will cause a retry, RuntimeError by default
    :raises exception: if all retries fail
    """
    def arg_decor(awaitable):
        @functools.wraps(awaitable)
        async def decor(*args, **kwargs):
            n_calls_todo = n_attempts
            while True:
                try:
                    return await awaitable(*args, **kwargs)
                except exception as x:
                    if n_calls_todo == n_attempts:
                        logger.debug(f"Calling {awaitable.__name__}(...) failed with '{x}'. Retrying ...")
                    elif n_calls_todo == 1:
                        logger.warning(f"... Calling {awaitable.__name__}(...) never succeeded within "
                                       f"'{n_attempts} attempts'.")
                        raise x
                    n_calls_todo -= 1
        return decor
    return arg_decor
