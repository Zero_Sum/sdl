# SDL by Sven Skobowsky

# To the extent possible under law, the person who associated CC0 with SDL
# has waived all copyright and related or neighboring rights to SDL.

# You should have received a copy of the CC0 legalcode along with this
# work.  If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.


import functools as fn


def as_callable(*, primed=False):
    """ Applying the interface of a callable to a coroutine, thus replacing the next/send interface.

    :example:
    >>> @as_callable(primed=True)
    ... def my_coroutine():
    ...     i = yield 666
    ...     yield; yield
    ...
    >>> call_me = my_coroutine() # no-priming required
    >>> call_me(42)  # instead of descriptive_name.send(42)
    >>> call_me()    # instead of next(descriptive_name)

    :param primed: whether or not to prime the coroutine on instantiation
    """
    def arg_decor(cor):
        @fn.wraps(cor)
        def decor(*args, **kwargs):
            c = cor(*args, **kwargs)
            if primed:
                next(c)
            return _CallableWrapper(c)
        return decor
    return arg_decor


class _CallableWrapper:
    def __init__(self, gen):
        self.__gen = gen

    def __call__(self, arg=None):
        return self.__gen.send(arg)
