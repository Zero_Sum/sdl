# SDL by Sven Skobowsky

# To the extent possible under law, the person who associated CC0 with SDL
# has waived all copyright and related or neighboring rights to SDL.

# You should have received a copy of the CC0 legalcode along with this
# work.  If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.


import asyncio
import heapq


async def as_completed_in_order(aws, **kwargs):
    """Await `aws` and yield their results in the same order but as soon as finished.

    If the awaitables finish out of order the function buffers results to keep the
    expected order. It's like a combination of asyncio.gather() (keeping order) and
    asyncio.as_completed (yield single result as soon as available).

    :example:
    >>> import asyncio
    >>> async def task(t):
    ...     for _ in range(t): await asyncio.sleep(0)
    ...     return t
    >>> async def run_tasks_as_finished():
    ...     return [await r for r in asyncio.as_completed([task(t) for t in [4, 2, 6, 8, 3]])]
    >>> # asyncio.run(run_tasks_as_finished())  # [2, 3, 4, 6, 8]  # TODO: enable example if Py>=3.7
    >>> async def run_tasks_in_order():
    ...     return [r async for r in as_completed_in_order([task(t) for t in [4, 2, 6, 8, 3]])]
    >>> # asyncio.run(run_tasks_in_order())  # [4, 2, 6, 8, 3]


    :param aws:     iterable of awaitables
    :param kwargs:  additional keyword arguments passed to asyncio.as_completed
    :return:        return values of the passed awaitables
    """
    async def numbered_aw(n, aw):
        return n, await aw

    buffer, nxt = [], 0
    for aw in asyncio.as_completed([numbered_aw(n, aw) for n, aw in enumerate(aws)], **kwargs):
        n, r = await aw
        if n != nxt:
            heapq.heappush(buffer, (n, r))
            continue
        nxt += 1
        yield r
        while buffer and buffer[0][0] == nxt:
            nxt += 1
            yield heapq.heappop(buffer)[1]
