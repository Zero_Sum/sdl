# SDL by Sven Skobowsky

# To the extent possible under law, the person who associated CC0 with SDL
# has waived all copyright and related or neighboring rights to SDL.

# You should have received a copy of the CC0 legalcode along with this
# work.  If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.


import functools as fn
import inspect


class ValueError(ValueError): pass


def is_positive(x):     return x > 0
def is_negative(x):     return x < 0
def is_not_negative(x): return not is_negative(x)
def is_not_positive(x): return not is_positive(x)


def valid_params(*arg_predicates, **kwarg_predicates):
    """Validate function arguments according to predicates and throw ValueError if one fails.

    The decorator re-uses the signature of the wrapped function for assgning the predicates. So,
    pass the predicates like you expect the values to be passed, with following exceptions:
        - assume all parameters to provide a default value (None)
        - variable arguments (positional or keyword aka args and kwargs) just accept a single
          predicate applied to all
        - If necessary, pass None to not have a predicate for some argument.

    :example:
    >>> @valid_params(is_not_negative)  # checks pos1 to be positive or zero
    ... def f(pos1, pos2, poskw, *args, kw, **kwargs): pass  # TODO: use slash-parameter here (.. pos2, /, poskw, ..) when Py>3.6
    ...
    >>> @valid_params(is_not_negative, is_positive)  # checks pos2 to be positive, pos1 as before
    ... def g(pos1, pos2, poskw, *args, kw, **kwargs): pass
    ...
    >>> @valid_params(None, is_positive)  # just checks pos2, pos1 is not validated
    ... def h(pos1, pos2, poskw, *args, kw, **kwargs): pass
    ...
    >>> @valid_params(poskw=is_not_negative)  # checks poskw
    ... def i(pos1, pos2, poskw, *args, kw, **kwargs): pass
    ...
    >>> @valid_params(args=is_negative)  # checks each parameter covered by args separately to be negative
    ... def j(pos1, pos2, poskw, *args, kw, **kwargs): pass
    ...
    >>> @valid_params(None, is_positive, kwargs=is_not_positive)  # checks pos2 and each parameter covered by kwargs
    ... def k(pos1, pos2, poskw, *args, kw, **kwargs): pass
    ...
    >>> @valid_params(len)  # any unary predicate will do it, built-ins ...
    ... def l(pos1, pos2, poskw, *args, kw, **kwargs): pass
    ...
    >>> @valid_params(kw=lambda x: x%2)  # ... lambdas (with the downside of having a cryptic error message) ...
    ... def m(pos1, pos2, poskw, *args, kw, **kwargs): pass
    ...
    >>> def between_0_and_1(x): return 0 <= x <= 1
    >>> @valid_params(share=between_0_and_1)  # ... or custom functions
    ... def n(foo, share): pass

    >>> n(42, 0.666)

    >>> n(42, 3.1415)
    Traceback (most recent call last):
        ...
    sdl.valid_params.ValueError: Parameter 'share=3.1415' does not apply to predicate 'between_0_and_1'.

    .. note:: Dealing with the different ways of calling (positional, kwargs, variable params)
    requires some effort. So for super cheap or time critical functions one should better validate
    manually.

    .. warning:: This decorator is not stackable, i.e. is not supposed to be used on decorated
    functions. This is because it inspects the signature of the wrapped function and decorators
    replace them.


    :param arg_predicates:
    :param kwarg_predicates:
    :return:
    """
    def arg_decor(func):
        signature = inspect.signature(func)
        preds = signature.bind_partial(*arg_predicates, **kwarg_predicates).arguments
        preds = {name: pred for name, pred in preds.items() if pred is not None}
        _create_variable_args_kwargs_predicate(signature, preds, inspect.Parameter.VAR_POSITIONAL, lambda vals: vals)
        _create_variable_args_kwargs_predicate(signature, preds, inspect.Parameter.VAR_KEYWORD   , lambda vals: vals.values())

        @fn.wraps(func)
        def decor(*args, **kwargs):
            values = signature.bind(*args, **kwargs).arguments
            for arg, pred in preds.items():
                try:
                    if not pred(values[arg]):
                        raise ValueError(f"Parameter '{arg}={values[arg]}' does not apply to predicate '{preds[arg].__name__}'.")
                except KeyError:
                    pass  # default value used
            return func(*args, **kwargs)

        return decor
    return arg_decor


def _find_var_parameter_name(signature, type):
    assert type in (inspect.Parameter.VAR_POSITIONAL, inspect.Parameter.VAR_KEYWORD)
    try:
        return next(name for name, description in signature.parameters.items()
                    if description.kind == type)
    except StopIteration:
        return ''


def _create_variable_args_kwargs_predicate(signature, predicates, type, get_values_from_container):
    """ wraps the predicate to be applied on container values individually """
    arg_name = _find_var_parameter_name(signature, type)
    if arg_name not in predicates.keys():
        return
    if len(predicates[arg_name]) != 1:
        raise ValueError(f"Multiple predicates for variable arguments '{arg_name}'.")
    first_pred = next(iter(get_values_from_container(predicates[arg_name])))
    predicates[arg_name] = lambda values: all(map(first_pred, get_values_from_container(values)))
