# SDL by Sven Skobowsky

# To the extent possible under law, the person who associated CC0 with SDL
# has waived all copyright and related or neighboring rights to SDL.

# You should have received a copy of the CC0 legalcode along with this
# work.  If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.


from setuptools import setup


setup(
    name='sdl',
    version='0.2.0-dev',
    url='https://gitlab.com/Zero_Sum/sdl',
    author='Sven Skobowsky',
    author_email='zerosum@posteo.org',
    description='Standard Decorator Library, general purpose decorators',
    classifiers=['License :: CC0 1.0 Universal (CC0 1.0) Public Domain Dedication'],
    packages=['sdl'],
    python_requires='>=3.6',
)
