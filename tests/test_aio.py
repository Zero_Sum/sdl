# SDL by Sven Skobowsky

# To the extent possible under law, the person who associated CC0 with SDL
# has waived all copyright and related or neighboring rights to SDL.

# You should have received a copy of the CC0 legalcode along with this
# work.  If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.


import asyncio
import unittest as ut
import unittest.mock as utm

import sdl.aio as aio


class AsyncMock(utm.MagicMock):
    # TODO: use utm.MagicMock and its await functionality once Python 3.8 is minimum version
    async def __call__(self, *args, **kwargs):
        return super().__call__(*args, **kwargs)


class Run(ut.TestCase):
    def setUp(self):
        self.async_test_was_run = False

    async def aio_func(self):
        self.async_test_was_run = True

    @aio.run
    async def test_decorated_test_is_run(self):
        await self.aio_func()

    @utm.patch('__main__.ord')
    @aio.run
    async def test_patched_decorated_test_is_run(self, _mock):
        await self.aio_func()

    def tearDown(self):
        self.assertTrue(self.async_test_was_run)


class StandardAsyncDecoratorTests:
    @aio.run
    async def test_calls_the_decorated_awaitable(self):
        func_called = False

        @self.decorator_under_test
        async def func():
            nonlocal func_called
            func_called = True

        await func()

        self.assertTrue(func_called)

    @aio.run
    async def test_decorator_passes_arguments_unaltered(self):
        received_args = None
        received_kwargs = None

        @self.decorator_under_test
        async def func(*args, **kwargs):
            nonlocal received_args
            nonlocal received_kwargs
            received_args = args
            received_kwargs = kwargs

        sent_args = 3.1415, 'hurz'
        sent_kwargs = dict(answer=42)

        await func(*sent_args, **sent_kwargs)

        self.assertTupleEqual(received_args, sent_args)
        self.assertDictEqual(received_kwargs, sent_kwargs)

    @aio.run
    async def test_decorator_does_not_alter_return_value(self):
        ret_val = 42

        @self.decorator_under_test
        async def func():
            return ret_val

        self.assertEqual(await func(), ret_val)

    @aio.run
    async def test_decorator_does_not_alter_function_name(self):
        async def func():
            pass

        decorated = self.decorator_under_test(func)

        self.assertEqual(func.__name__, decorated.__name__)


class ThrottleStandardTests(ut.TestCase, StandardAsyncDecoratorTests):
    def setUp(self):
        self.decorator_under_test = aio.throttle(0)


class ThrottleInstantiation(ut.TestCase):
    def test_number_of_calls_less_than_0_cause_are_not_accepted(self):
        @aio.throttle(60, 1)
        def f(): pass

        with self.assertRaisesRegex(ValueError, 'calls'):
            @aio.throttle(60, 0)
            def f(): pass

        with self.assertRaisesRegex(ValueError, 'calls'):
            @aio.throttle(60, -1)
            def f(): pass


class ThrottleSingleCallPerTime(ut.TestCase):
    def setUp(self):
        self.delay_s = 5

        @aio.throttle(self.delay_s)
        async def throttled_func(): pass

        self.throttled_func = throttled_func

        patcher = utm.patch('sdl.aio.asyncio.sleep', new_callable=AsyncMock)
        self.addCleanup(patcher.stop)
        self.sleep = patcher.start()

        patcher = utm.patch('sdl.aio.time.time', autospec=True)
        self.addCleanup(patcher.stop)
        self.time = patcher.start()

        self.tp = 202108.21
        self.time.return_value = self.tp

    @aio.run
    async def test_first_call_is_not_delayed(self):
        await self.throttled_func()

        self.sleep.assert_not_called()

    @aio.run
    async def test_follow_up_call_done_too_fast_will_be_delayed_by_remaining_time_to_correctly_timed_call(self):
        await self.throttled_func()
        next_ok_tp = self.tp + self.delay_s
        too_soon_tp = next_ok_tp - 0.666 * self.delay_s
        self.time.return_value = too_soon_tp

        await self.throttled_func()

        self.assertAlmostEqual(self.sleep.call_args[0][0], next_ok_tp - too_soon_tp)

    @aio.run
    async def test_calls_slower_than_min_delay_are_not_delay_calls_further(self):
        await self.throttled_func()
        self.time.return_value += self.delay_s

        await self.throttled_func()

        self.sleep.assert_not_called()

    @aio.run
    async def test_delays_of_multiple_calls_will_be_the_multiple_of_the_expected_period(self):
        n_calls = range(10)
        # 1st call waits 0*delay, 2nd call waits 1*delay, 3rd call waits 2*delay, ....
        expected_total_delay = sum(n for n in n_calls) * self.delay_s

        await asyncio.gather(*[self.throttled_func() for _ in n_calls])

        total_delays = sum(args[0][0] for args in self.sleep.call_args_list)
        self.assertAlmostEqual(total_delays, expected_total_delay)

    @aio.run
    async def test_separately_decorated_functions_do_not_interfere(self):
        @aio.throttle(1.5 * self.delay_s)
        async def other_throttled_func(): pass

        await self.throttled_func()
        self.sleep.assert_not_called()

        self.time.return_value += self.delay_s
        await other_throttled_func()
        await self.throttled_func()
        self.sleep.assert_not_called()

        self.time.return_value += self.delay_s
        await self.throttled_func()
        self.sleep.assert_not_called()
        await other_throttled_func()
        self.sleep.assert_called()


class ThrottleMultipleCallsPerTime(ut.TestCase):
    def setUp(self):
        self.delay_s = 60
        self.calls_in_period = 5

        @aio.throttle(self.delay_s, self.calls_in_period)
        async def throttled_func(): pass

        self.throttled_func = throttled_func

        patcher = utm.patch('sdl.aio.asyncio.sleep', new_callable=AsyncMock)
        self.addCleanup(patcher.stop)
        self.sleep = patcher.start()

        patcher = utm.patch('sdl.aio.time.time', autospec=True)
        self.addCleanup(patcher.stop)
        self.time = patcher.start()

        self.tp = 202108.21
        self.time.return_value = self.tp

    @aio.run
    async def test_all_calls_are_done_in_a_burst(self):
        await asyncio.gather(*[self.throttled_func() for _ in range(self.calls_in_period)])
        self.sleep.assert_not_called()

        await asyncio.gather(*[self.throttled_func() for _ in range(self.calls_in_period)])
        self.sleep.assert_called()
        self.assertListEqual([call[0][0] for call in self.sleep.call_args_list],
                             [self.delay_s] * self.calls_in_period)


class RetryStandardTests(ut.TestCase, StandardAsyncDecoratorTests):
    def setUp(self):
        self.decorator_under_test = aio.retry()


class Retry(ut.TestCase):
    @aio.run
    async def test_on_RuntimeError_decorated_awaitable_is_called_again(self):
        n_calls = 0

        @aio.retry()
        async def f():
            nonlocal n_calls
            n_calls += 1
            if n_calls == 1:
                raise RuntimeError('moep')

        await f()

        self.assertEqual(n_calls, 2)

    @aio.run
    async def test_other_exception_types_raised_do_not_cause_repetition_and_are_propagated(self):
        n_calls = 0

        @aio.retry()
        async def f():
            nonlocal n_calls
            n_calls += 1
            if n_calls == 1:
                raise EOFError('moep')

        with self.assertRaisesRegex(EOFError, 'moep'):
            await f()

        self.assertEqual(n_calls, 1)

    @aio.run
    async def test_custom_exception_type_replaces_RuntimeError_if_provided(self):
        class MyException(Exception): pass
        n_calls = 0

        @aio.retry(exception=MyException)
        async def f():
            nonlocal n_calls
            n_calls += 1
            if n_calls == 1:
                raise MyException('moep')

        await f()

        self.assertEqual(n_calls, 2)

    @aio.run
    async def test_number_of_attempts_is_limited_to_specified_number(self):
        n_calls = 0
        n_calls_expected = 5

        @aio.retry(n_attempts=n_calls_expected)
        async def f():
            nonlocal n_calls
            n_calls += 1
            if n_calls < 2 * n_calls_expected:  # just to not loop infinitely
                raise RuntimeError('moep')

        try:
            await f()
        except RuntimeError:
            pass
        finally:
            self.assertEqual(n_calls, n_calls_expected)

    @aio.run
    async def test_if_all_attempts_fail_retry_exception_is_propagated(self):
        @aio.retry(n_attempts=3)
        async def f():
            raise RuntimeError('moep')

        with self.assertRaisesRegex(RuntimeError, 'moep'):
            await f()
