# SDL by Sven Skobowsky

# To the extent possible under law, the person who associated CC0 with SDL
# has waived all copyright and related or neighboring rights to SDL.

# You should have received a copy of the CC0 legalcode along with this
# work.  If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.


import unittest as ut
import unittest.mock as utm

from sdl.cor import as_callable


class AsCallable(ut.TestCase):
    def test_using_callable_yields_same_result_as_advancing_the_plain_generator(self):
        def plain():
            yield from range(1, 33, 7)
        Callable = as_callable()(plain)

        c = Callable()
        result = []
        try:
            while True:
                result.append(c())
        except StopIteration:
            pass

        expected = list(plain())
        self.assertListEqual(result, expected)

    def test_using_callable_with_values_yields_same_result_as_sending_those_values_to_the_plaint_generator(self):
        def plain():
            i, j = 0, 0
            while True:
                j = yield i + j
                i += 1
        Callable = as_callable()(plain)
        values_to_send = (None, 24, 8, 5, 2, 47, 98, 4, 3, 4)
        
        c = Callable()
        result = tuple(c(v) for v in values_to_send)
        
        p = plain()
        expected = tuple(p.send(v) for v in values_to_send)
        self.assertTupleEqual(result, expected)

    def test_callable_primes_coroutine_if_prime_is_true(self):
        def plain():
            i = yield 0
            i = yield i
        Callable = as_callable()(plain)
        PrimedCallable = as_callable(primed=True)(plain)

        with self.assertRaises(TypeError):
            Callable()(42)

        PrimedCallable()(42)

    def test_prime_parameter_must_be_kwarg(self):
        with self.assertRaises(TypeError):
            as_callable(True)(lambda: True)

    def test_decorator_retains_name(self):
        def a_somewhat_longer_function_name_to_reduce_accidental_matches():
            pass
        Callable = as_callable()(a_somewhat_longer_function_name_to_reduce_accidental_matches)

        self.assertEqual(a_somewhat_longer_function_name_to_reduce_accidental_matches.__name__,
                         Callable.__name__)

    def test_init_parameters_are_forwarded_to_coroutine(self):
        cor_mock = utm.Mock()
        Callable = as_callable()(cor_mock)
        some_args = [42, 'foo']
        some_kwargs = dict(pi=3.1415)

        Callable(*some_args, **some_kwargs)

        cor_mock.assert_called_with(*some_args, **some_kwargs)
