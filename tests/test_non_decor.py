# SDL by Sven Skobowsky

# To the extent possible under law, the person who associated CC0 with SDL
# has waived all copyright and related or neighboring rights to SDL.

# You should have received a copy of the CC0 legalcode along with this
# work.  If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.

import asyncio
import unittest as ut
import unittest.mock as utm

import sdl.non_decor as ndec
import sdl.aio as aio


class AsCompletedInOrder(ut.TestCase):
    @aio.run
    async def test_yields_the_same_results_as_common_as_completed(self):
        async def waste_time(t):
            for _ in range(t):
                await asyncio.sleep(0)
            return t
        durations = [2, 4, 1, 0, 3, 6, 5]
        expected = {await r for r in asyncio.as_completed([waste_time(d) for d in durations])}

        actual = {r async for r in ndec.as_completed_in_order([waste_time(d) for d in durations])}

        self.assertSetEqual(actual, expected)

    @aio.run
    async def test_yields_the_results_in_the_order_of_passed_awaitables(self):
        async def waste_time(t):
            for _ in range(t):
                await asyncio.sleep(0)
            return t
        durations = [2, 4, 1, 0, 3, 6, 5]

        results = [r async for r in ndec.as_completed_in_order([waste_time(d) for d in durations])]

        self.assertListEqual(durations, results)

    @aio.run
    async def test_yields_results_as_soon_as_available(self):
        durations = [2, 4, 1, 0, 3, 6, 5]
        finished = [False] * len(durations)

        async def waste_time(n, t):
            for _ in range(2*t):  # TODO: think of a better test, this one seems to have timing issues
                await asyncio.sleep(0)
            finished[n] = True
            return t

        assert durations[0] != max(durations)
        async for r in ndec.as_completed_in_order([waste_time(n, d) for n, d in enumerate(durations)]):
            if r == max(durations):
                break  # at that point all aws should have finished
            self.assertTrue( not all(finished) )

    @utm.patch('asyncio.as_completed')
    @aio.run
    async def test_forwards_kwargs_to_asyncio_as_completed(self, mock):
        expected_kwargs = dict(pi=3.1415, euler=2.7182)
        async def f(): pass

        async for _ in ndec.as_completed_in_order([f()], **expected_kwargs):
            pass

        mock.assert_called_once()
        fwd_kwargs = mock.call_args[1]
        self.assertEqual(fwd_kwargs, {**expected_kwargs, **fwd_kwargs})
