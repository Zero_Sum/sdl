# SDL by Sven Skobowsky

# To the extent possible under law, the person who associated CC0 with SDL
# has waived all copyright and related or neighboring rights to SDL.

# You should have received a copy of the CC0 legalcode along with this
# work.  If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.


import unittest as ut

import sdl.valid_params as vp


class StandardDecoratorTests:
    def test_calls_the_decorated_function(self):
        func_called = False

        @self.decorator_under_test
        def func():
            nonlocal func_called
            func_called = True

        func()

        self.assertTrue(func_called)

    def test_decorator_passes_arguments_unaltered(self):
        received_args = None
        received_kwargs = None

        @self.decorator_under_test
        def func(*args, **kwargs):
            nonlocal received_args
            nonlocal received_kwargs
            received_args = args
            received_kwargs = kwargs

        sent_args = 3.1415, 'hurz'
        sent_kwargs = dict(answer=42)

        func(*sent_args, **sent_kwargs)

        self.assertTupleEqual(received_args, sent_args)
        self.assertDictEqual(received_kwargs, sent_kwargs)

    def test_decorator_does_not_alter_return_value(self):
        ret_val = 42

        @self.decorator_under_test
        def func():
            return ret_val

        self.assertEqual(func(), ret_val)

    def test_decorator_does_not_alter_function_name(self):
        def func():
            pass

        decorated = self.decorator_under_test(func)

        self.assertEqual(func.__name__, decorated.__name__)


class ValidateParameterStandardTests(ut.TestCase, StandardDecoratorTests):
    def setUp(self):
        self.decorator_under_test = vp.valid_params()


class ParameterChecker(ut.TestCase):
    @staticmethod
    def is_not_negative(x): return x > 0

    @staticmethod
    def is_odd(x): return x % 2

    @staticmethod
    def is_float(x): return isinstance(x, float)

    def test_single_positional_argument_is_checked_according_to_given_predicate(self):
        @vp.valid_params(self.is_not_negative)
        def func(a): pass

        func(42)
        with self.assertRaises(vp.ValueError):
            func(-1)

    def test_raised_exception_is_a_ValueError(self):
        @vp.valid_params(self.is_not_negative)
        def func(a): pass

        with self.assertRaises(ValueError):
            func(-1)

    def test_positional_arguments_are_checked_to_the_according_positional_predicates(self):
        @vp.valid_params(self.is_not_negative, self.is_odd, self.is_float)
        def func(a, b, c): pass

        func(42, 1337, 3.1415)
        with self.assertRaisesRegex(vp.ValueError, 'not_negative'):
            func(-666, 1337, 3.1415)
        with self.assertRaisesRegex(vp.ValueError, 'odd'):
            func(42, 42, 3.1415)
        with self.assertRaisesRegex(vp.ValueError, 'float'):
            func(42, 1337, 'hurz')

    def test_positional_arguments_are_only_not_checked_if_a_predicate_is_given_for_it(self):
        @vp.valid_params(self.is_not_negative)
        def func(a, b, c): pass

        with self.assertRaises(vp.ValueError):
            func(-666, 42, 'hurz')
        func(42, -666, 'hurz')  # does not raise

    def test_check_for_positional_arguments_can_be_avoided_by_passing_None(self):
        @vp.valid_params(None, self.is_not_negative)
        def func(a, b): pass

        func(-666, 42)  # does not raise

    def test_predicates_can_be_matched_as_named_argument(self):
        @vp.valid_params(b=self.is_not_negative)
        def func(a, b): pass

        with self.assertRaises(vp.ValueError):
            func(42, -666)

    def test_default_values_are_not_validated(self):
        @vp.valid_params(self.is_not_negative)
        def func(a=-666): pass

        func()

    def test_variable_args_can_be_validated_with_a_common_predicate_applied_to_all(self):
        @vp.valid_params(None, self.is_not_negative)
        def func(a, *args): pass

        func(-1, 2, 3)
        with self.assertRaisesRegex(vp.ValueError, "args"):
            func(1, -2, 3)
        with self.assertRaisesRegex(vp.ValueError, "args"):
            func(1, 2, -3)

    def test_variable_kwargs_can_be_validated_with_a_common_predicate_applied_to_all(self):
        @vp.valid_params(kwargs=self.is_not_negative)
        def func(*, a, **kwargs): pass

        func(a=-1, b=2, c=3)
        with self.assertRaisesRegex(vp.ValueError, "kwargs"):
            func(a=1, b=-2, c=3)
        with self.assertRaisesRegex(vp.ValueError, "kwargs"):
            func(a=1, b=2, c=-3)

    def test_multiple_predicates_for_variable_positional_arguments_are_declined(self):
        with self.assertRaisesRegex(vp.ValueError, "[Mm]ultiple predicates.*'args'"):
            @vp.valid_params(self.is_not_negative, self.is_not_negative)
            def func(*args): pass

    def test_multiple_predicates_for_variable_keyword_arguments_are_declined(self):
        with self.assertRaisesRegex(vp.ValueError, "[Mm]ultiple predicates.*'kwargs'"):
            @vp.valid_params(one=self.is_not_negative, two=self.is_not_negative)
            def func(**kwargs): pass

    def test_name_of_argument_and_of_failing_predicate_is_passed_to_exception(self):
        @vp.valid_params(self.is_not_negative)
        def func(qwerty): pass

        with self.assertRaisesRegex(vp.ValueError, "qwerty.*not_negative"):
            func(-1)
